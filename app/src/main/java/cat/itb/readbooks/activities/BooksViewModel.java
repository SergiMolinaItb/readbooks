package cat.itb.readbooks.activities;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class BooksViewModel extends ViewModel {
    public static List<Books> books = new ArrayList<>();


    public BooksViewModel() {
        if (books.isEmpty()) {
            books = new ArrayList<Books>() {{
                add(new Books("sasas", "sasas", "Read", 1));
                add(new Books("sasas", "sasas", "Reading", 5));
                add(new Books("sasas", "sasas", "Read", 2));
                add(new Books("sasas", "sasas", "Want To Read", 5));
                add(new Books("sasas", "sasas", "Read", 5));
                add(new Books("sasas", "sasas", "Read", 5));
                add(new Books("sasas", "sasas", "Read", 5));
                add(new Books("sasas", "sasas", "Read", 5));
                add(new Books("sasas", "sasas", "Read", 5));
                add(new Books("sasas", "sasas", "Read", 5));
            }};
        }
    }
}
