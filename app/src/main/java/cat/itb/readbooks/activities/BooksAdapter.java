package cat.itb.readbooks.activities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.readbooks.R;

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.BooksViewHolder> {
    public List<Books> books = BooksViewModel.books;
    public static int number;

    public BooksAdapter(List<Books> books) {
        this.books = books;
    }

    @NonNull
    @Override
    public BooksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.books_list_item, parent, false);

        return new BooksViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BooksViewHolder holder, int position) {
        holder.bindData(books.get(position));
    }


    public int getItemCount() {
        return books.size();
    }

    class BooksViewHolder extends RecyclerView.ViewHolder {
        TextView authorName;
        TextView titleName;
        TextView status;
        RatingBar ratingBar;
        Button add;

        public BooksViewHolder(@NonNull View itemView) {
            super(itemView);

            titleName = itemView.findViewById(R.id.titleName);
            authorName = itemView.findViewById(R.id.authorName);
            status = itemView.findViewById(R.id.statusText);
            ratingBar = itemView.findViewById(R.id.ratingBar);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    number = 0;
                    NavDirections navDirections = BooksListFragmentDirections.actionBooksListFragmentToBooksFragment(books.get(getAdapterPosition()));
                    Navigation.findNavController(v).navigate(navDirections);
                }
            });

        }

        public void bindData(Books books) {
            for (int i = 0; i < getItemCount(); i++) {
                titleName.setText(books.getTitle());
                authorName.setText(books.getAuthor());
                status.setText(books.getStatus());
                if (books.getStatus().equals("Read")) {
                    ratingBar.setVisibility(View.VISIBLE);
                    ratingBar.setRating(books.getRate());
                } else {
                    ratingBar.setVisibility(View.GONE);
                }
            }
        }
    }
}