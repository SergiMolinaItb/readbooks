package cat.itb.readbooks.activities;

import android.os.Parcel;
import android.os.Parcelable;

public class Books implements Parcelable {
    private String title;
    private String author;
    private String status;
    private int rate;

    public Books(String title, String author, String status, int rate) {
        this.title = title;
        this.author = author;
        this.status = status;
        this.rate = rate;
    }

    protected Books(Parcel in) {
        title = in.readString();
        author = in.readString();
        rate = in.readInt();
    }

    public static final Creator<Books> CREATOR = new Creator<Books>() {
        @Override
        public Books createFromParcel(Parcel in) {
            return new Books(in);
        }

        @Override
        public Books[] newArray(int size) {
            return new Books[size];
        }
    };

    public Books() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(author);
    }
}
