package cat.itb.readbooks.activities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import java.util.List;

import cat.itb.readbooks.R;

public class BooksFragment extends Fragment {

    List<Books> books = BooksViewModel.books;
    Books book;
    EditText titleText;
    EditText authorText;
    Spinner spinner;
    RatingBar ratingBar;
    Button add;

    String title;
    String author;
    String spinnerText;
    int ratingNum;
    int num = BooksAdapter.number;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity.fragment = 1;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.books_fragment, container, false);

        titleText = v.findViewById(R.id.editTextTitle);
        authorText = v.findViewById(R.id.editTextAuthor);
        spinner = v.findViewById(R.id.spinner);
        ratingBar = v.findViewById(R.id.ratingBar);
        add = v.findViewById(R.id.button2);

        if (getArguments() != null) book = getArguments().getParcelable("books");

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);

                if ((selection).equals("Read")) ratingBar.setVisibility(View.VISIBLE);
                else ratingBar.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (spinner.getSelectedItem().equals("Read")) ratingBar.setVisibility(View.VISIBLE);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (titleText.getText().toString().isEmpty() || authorText.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(),"Introdueix Totes Les Dades.", Toast.LENGTH_SHORT).show();
                } else {
                    title = titleText.getText().toString();
                    author = authorText.getText().toString();
                    spinnerText = spinner.getSelectedItem().toString();
                    if (ratingBar.getVisibility() == View.GONE) ratingNum = 0;
                    else ratingNum = ratingBar.getNumStars();
                    books.add(new Books(title, author, spinnerText, ratingNum));
                }
            }
        });


        return v;
    }
}
